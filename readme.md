# Neo4j

Base template to create a local instance of neo4j. Community image, shared folders. Basic stuff to start playing with graph database.

## How to start

```bash
> git clone https://gitlab.com/d4497/docker/neo4j.git
> cd neo4j
> docker compose up -d
```

Go the browser: `http://localhost:7474/browser/`

## Folder structure

```
- neo4j
	- conf
	- data
	- import
	- logs
	- plugins
- docker-compose.yaml
```

## References

* https://thibaut-deveraux.medium.com/how-to-install-neo4j-with-docker-compose-36e3ba939af0
* Env: https://neo4j.com/docs/operations-manual/current/reference/configuration-settings/